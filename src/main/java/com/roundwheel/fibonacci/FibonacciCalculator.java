package com.roundwheel.fibonacci;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;
import java.math.RoundingMode;

import org.apache.commons.lang3.time.StopWatch;

/**
 * {@code FibonacciCalculator} demonstrates that dividing 1 by 999999999999999999999998999999999999999999999999
 * resutls in a number with the Fibonacci sequence embedded within it.
 * 
 * @author Yvon X Comeau
 * 
 * @see http://goo.gl/H7apcI
 */
public class FibonacciCalculator {

	/**
	 * Displays the Fibonacci series as a raw string using the 1 / 99999999 approach, and then formatted for clarity.
	 */
	public FibonacciCalculator() {
		StopWatch stopWatch = new StopWatch();

		stopWatch.start();
		BigDecimal one = new BigDecimal("1");
		BigDecimal nines = new BigDecimal("999999999999999999999998999999999999999999999999");
		BigDecimal fib = one.divide(nines, new MathContext(1000, RoundingMode.HALF_EVEN));

		stopWatch.stop();

		System.out.println("Raw Fibonacci string:\n\n" + fib.toString() + "\n");
		System.out.println("Calculation took " + stopWatch.getNanoTime() + " nanoseconds");
		char[] chars = fib.toString().toCharArray();
		int j = 2;

		for (int i = 0; i < chars.length; i++) {
			if (i < 2) {
				// skip these
			} else if (i == 2) {
				System.out.print("\n" + j + "\t");
			} else if ( (i - 2) % 24 == 0) {
				j ++;
				System.out.print("\n" + j + "\t");
			} else {
				System.out.print(chars[i]);
			}
		}
	}

	/***
	 * Calculates the fibonnaci value for a given number in the series and displays performance metrics.
	 *
	 * @param pos  the position in the series
	 */
	private void calculateFib(int pos) {
		StopWatch stopWatch = new StopWatch();
		stopWatch.start();
		BigInteger fib = fibonacci(pos);
		stopWatch.stop();
		System.out.println(pos + "\t" + fib + "\t\t calculation took " + stopWatch.getNanoTime() + " nanoseconds");
	}

	/***
	 * Calculates the fibonnaci value for a given number in the series using a non-recursive loop.
	 *
	 * @param   pos  the position in the series
	 * @return  the fibonnaci value for this number
	 */
	public BigInteger fibonacci(int pos) {
	    BigInteger a = BigInteger.ONE;
	    BigInteger b = BigInteger.ONE;
	    while (pos-- > 2) {
	        BigInteger c = a.add(b);
	        a = b;
	        b = c;
	    }
	    return b;
	}

	/***
	 * Run the calculator and then display a few sample fibbonacci numbers to check for correctness.
	 *
	 * @param args
	 */
	public static void main(String[] args) {
		FibonacciCalculator calculator = new FibonacciCalculator();

		System.out.println("\n---------------");

		calculator.calculateFib(7);

		calculator.calculateFib(15);

		calculator.calculateFib(26);

		calculator.calculateFib(35);

		calculator.calculateFib(42);
	}
}